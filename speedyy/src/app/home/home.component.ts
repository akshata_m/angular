import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

   color = 'primary';
   value = 10;
   bufferValue = 75;

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  onClick() {
      alert('Go to the next Page!');
      this.router.navigate(['fssai-details']);
  }

}
