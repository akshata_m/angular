import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fssai-details',
  templateUrl: './fssai-details.component.html',
  styleUrls: ['./fssai-details.component.css']
})
export class FssaiDetailsComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  onClickNext() {
    alert('Go to the back Page!');
    this.router.navigate(['home-page']);
  }

  onClickBack() {
    alert('Go to the next Page!');
    this.router.navigate(['home-page']);
  }



}
