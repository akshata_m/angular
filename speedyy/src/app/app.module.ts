import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';  
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatNativeDateModule} from '@angular/material/core';
import {MaterialExampleModule} from '../material.module';
import { FssaiDetailsComponent } from './fssai-details/fssai-details.component';
import { RouterModule, Routes } from '@angular/router';
import { ComissionComponent } from './comission/comission.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { MenuComponent } from './menu/menu.component';
import { OwnerDetailsComponent } from './owner-details/owner-details.component';
import { GstDetailsComponent } from './gst-details/gst-details.component';
import { BankDetailsComponent } from './bank-details/bank-details.component';
import { ESignComponent } from './e-sign/e-sign.component';
import { OnboardingFormComponent } from './onboarding-form/onboarding-form.component';

const appRoutes: Routes = [
  { path: 'home-page', component: HomeComponent },
  { path: 'fssai-details', component: FssaiDetailsComponent },
  { path: 'comission', component: ComissionComponent },
  { path: 'owner-details', component: OwnerDetailsComponent },
  { path: 'gst-details', component: GstDetailsComponent },
  { path: 'bank-details', component: BankDetailsComponent },
  { path: 'e-sign', component: ESignComponent },
  { path: 'onboarding-form', component: OnboardingFormComponent }
  
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    FssaiDetailsComponent,
    ComissionComponent,
    MenuComponent,
    OwnerDetailsComponent,
    GstDetailsComponent,
    BankDetailsComponent,
    ESignComponent,
    OnboardingFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MaterialExampleModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
