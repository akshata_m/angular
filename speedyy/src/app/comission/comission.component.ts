import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comission',
  templateUrl: './comission.component.html',
  styleUrls: ['./comission.component.css']
})
export class ComissionComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  onClickAccept() {
    alert('Go to the back Page!');
    this.router.navigate(['owner-details']);
  }

  onClickDecline() {
    alert('Go to the next Page!');
    this.router.navigate(['fssai-details']);
  }

}
